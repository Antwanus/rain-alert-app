import requests
from twilio.rest import Client

ACCOUNT_SID = 'AC7d02f87b44eccbbb224e80185b92c74a'
AUTH_TOKEN = '8a606e22dd6c10feca1336bef781a9a5'
API_KEY = 'dc0857d05cdb827318a2b741bb53ee79'
API_HOST = f'http://api.openweathermap.org/data/2.5/onecall'

# GET weather data
params = {
    'lat': 51.2213,
    'lon': 4.4051,
    'appid': API_KEY,
    'exclude': 'current,minutely,daily'
}
response = requests.get(API_HOST, params=params)
response.raise_for_status()
response_data = response.json()
weather_slice = response_data['hourly'][:12]

for hour_data in weather_slice:
    if hour_data['weather'][0]['id'] < 700:
        # Send SMS notification
        client = Client(ACCOUNT_SID, AUTH_TOKEN)
        message = client.messages.create(
            body="Looks like it's going to rain, bring an ☂️",
            from_='+13028657262',
            to='+32497570403'
        )
